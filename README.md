Qpbl
====
Qt-based code-snippets for the Pebble smartwatch

Featuring
=========
  - A tool to talk to the watch
    - ```qpbl ping```
    - ```qpbl notify "a sender" "a subject" "a body"```

Build
=====

Requirements
------------
 - Qt5 >= 5.2
 - A compiler qith C++11 support
 - QtBluetooth

Other than that:  
```qmake && make```

Tests
=====
There are actually some tests
Run ```make check``` to run them

TODO
====
- Handle disconnects
- Handle received data
- Implement application messages

Wishlist
========
- Feature parity with the ```pebble``` tool from the SDK

