#include "pebblemessage.h"

Q_LOGGING_CATEGORY(qpblmessage, "qpbl.pebblemessage");

PebbleMessage::PebbleMessage():
    m_endpoint(0),
    m_timestamp(QDateTime::currentDateTime())
{
}

PebbleMessage::PebbleMessage(uint id):
    m_endpoint(id),
    m_timestamp(QDateTime::currentDateTime())
{

}

QByteArray PebbleMessage::pack()
{
    QByteArray ret;
    QDataStream ds(&ret, QIODevice::ReadWrite);
    ds.setByteOrder(QDataStream::BigEndian);

    quint16 len = 0;

    if(m_endpoint == Endpoints::PING) {
        qCDebug(qpblmessage) << "Packing a ping";
        len = 5; // Always for a PING, padding + u32
        ds << len << m_endpoint << quint8(0) << m_cookie;
    } else if(m_endpoint == Endpoints::NOTIFICATION) {
        qCDebug(qpblmessage) << "Packing a notification";
        QByteArray data = packMessageData(QStringList() << m_sender << m_body << QString::number(m_timestamp.toMSecsSinceEpoch()) << m_subject);
        ds << (quint16) data.size();
        ds << m_endpoint;
        ret.append(data);
    }

    return ret;
}

void PebbleMessage::setEndpoint(uint id)
{
    m_endpoint = id;
}

void PebbleMessage::setCookie(quint32 cookie)
{
    m_cookie = cookie;
}

void PebbleMessage::sender(QString sender)
{
    m_sender = sender;
}

void PebbleMessage::subject(QString subject)
{
    m_subject = subject;
}

void PebbleMessage::body(QString body)
{
    m_body = body;
}

void PebbleMessage::timestamp(QDateTime timestamp)
{
    m_timestamp = timestamp;
}

QByteArray PebbleMessage::packMessageData(QStringList data) {
    return packMessageData(data, 0);
}

QByteArray PebbleMessage::packMessageData(QStringList data, quint8 lead)
{
//    Reference from libpebble:
//    import time
//    from struct import pack
//    def _pack_message_data(lead, parts):
//        pascal = map(lambda x: x[:255], parts)
//        d = pack("b" + reduce(lambda x,y: str(x) + "p" + str(y), map(lambda x: len(x) + 1, pascal)) + "p", lead, *pascal)
//        return d
//    print _pack_message_data(0, ["asmw", "this is the message body", str(int(time.time())*1000), "this is the subject"])

    QByteArray ret;
    ret.append(lead);
    // The lead byte counts

    QByteArray tmp;
    foreach(QString s, data) {
        tmp = s.toLocal8Bit();
        tmp.truncate(255);
        ret.append((quint8)tmp.size());
        ret.append(tmp);
    }

    return ret;
}
