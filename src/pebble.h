#ifndef PEBBLE_H
#define PEBBLE_H

#include <QObject>
#include <QBluetoothAddress>
#include <QBluetoothSocket>
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(qpbl)

class Pebble : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ready MEMBER m_ready NOTIFY readyChanged)

public:
    explicit Pebble(QObject *parent = 0);
    explicit Pebble(QString id, QObject *parent = 0);
    explicit Pebble(QBluetoothAddress addr, QObject *parent = 0);

signals:
    void readyChanged(bool ready);
    void connected();
    void disconnected();

public slots:
    void connect();
    void connect(QBluetoothAddress address);
    void disconnect();
    void ping(quint32 cookie = 0);
    void notify(QString sender, QString subject, QString body);

private slots:
    void onConnected();
    void onDisconnected();
    void onData();

private:
    QBluetoothSocket m_socket;
    QBluetoothAddress m_address;
    bool m_ready;

    bool readMessage();

    void initConnections();
};

#endif // PEBBLE_H
