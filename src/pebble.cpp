#include <QDebug>
#include <QTimer>

#include "pebble.h"
#include "pebblemessage.h"

Q_LOGGING_CATEGORY(qpbl, "qpbl.pebble");

Pebble::Pebble(QString id, QObject *parent) :
    QObject(parent),
    m_socket(QBluetoothServiceInfo::RfcommProtocol),
    m_address("00:17:E9:DB:" + id.mid(0,2) + ":" + id.mid(2,2)),
    m_ready(false)
{
    // The Pebble ID seems to be the last two bytes of the bluetooth ID
    // I'm not sure if the prefix is ubiquitous or if i.e. chips of other manufacturers are used

    initConnections();
}

Pebble::Pebble(QBluetoothAddress addr, QObject *parent) :
    QObject(parent),
    m_socket(QBluetoothServiceInfo::RfcommProtocol),
    m_address(addr),
    m_ready(false)
{
    initConnections();
}


void Pebble::connect()
{
    qCDebug(qpbl) << "connect called";
    qCDebug(qpbl) << m_socket.state();
    if(m_socket.state() == QBluetoothSocket::UnconnectedState) {
        qDebug() << "connecting to address" << m_address.toString() << "on channel 1";
        m_socket.connectToService(m_address, 1);
    }
}

void Pebble::connect(QBluetoothAddress address)
{
    m_address = address;
    connect();
}

void Pebble::disconnect()
{
    if(m_socket.state() == QBluetoothSocket::ConnectedState) {
        m_socket.close();
    }
}

void Pebble::ping(quint32 cookie)
{
    qCDebug(qpbl) << "ping, cookie:" << cookie;

    PebbleMessage m(PebbleMessage::Endpoints::PING);
    m.setCookie(cookie);

    // Can't use a char. DataStream serializes it to more than one byte?!?
    qCDebug(qpbl) << m.pack().toHex();
    m_socket.write(m.pack());
}

void Pebble::notify(QString sender, QString subject, QString body)
{
    qCDebug(qpbl) << "notify";

    PebbleMessage m(PebbleMessage::Endpoints::NOTIFICATION);
    m.sender(sender);
    m.subject(subject);
    m.body(body);

    // Can't use a char. DataStream serializes it to more than one byte?!?
    qCDebug(qpbl) << m.pack().toHex();
    m_socket.write(m.pack());
}

void Pebble::onConnected()
{
    qCDebug(qpbl) << "connected";
    connected();
}

void Pebble::onDisconnected()
{
    qCDebug(qpbl) << "disconnected";
    disconnected();
}

void Pebble::onData()
{
    qCDebug(qpbl) << "data available";
    while(m_socket.bytesAvailable() > 0) {
        readMessage();
    }
}

bool Pebble::readMessage()
{
    QDataStream ds(&m_socket);
    // read the size
    quint16 len;
    quint16 endpoint;
    ds >> len;
    ds >> endpoint;
    qCDebug(qpbl) << "Length:" << len;
    qCDebug(qpbl) << "Endpoint:" << endpoint;
    if(len > 0) {
        if(m_socket.bytesAvailable() < len) {
            qCDebug(qpbl) << "Not enough data, purging socket";
            m_socket.readAll();
            return false;
        }
        QByteArray data = m_socket.read(len);
        qCDebug(qpbl) << "Message:" << data.toHex();
    }
    if(endpoint == 49) {
        if(!m_ready) {
            m_ready = true;
            readyChanged(m_ready);
        }
    }
    return true;
}

void Pebble::initConnections()
{
    QObject::connect(&m_socket, &QBluetoothSocket::connected, this, &Pebble::onConnected);
    QObject::connect(&m_socket, &QBluetoothSocket::disconnected, this, &Pebble::onDisconnected);
    QObject::connect(&m_socket, &QBluetoothSocket::readyRead, this, &Pebble::onData);
}
