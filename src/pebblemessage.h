#ifndef PEBBLEMESSAGE_H
#define PEBBLEMESSAGE_H

#include <QByteArray>
#include <QDateTime>
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(qpblmessage)

class PebbleMessage
{
public:
    struct Endpoints {
        enum EndpointNums {
            FIRMWARE = 1,
            TIME = 11,
            VERSION = 16,
            PHONE_VERSION = 17,
            SYSTEM_MESSAGE = 18,
            MUSIC_CONTROL = 32,
            PHONE_CONTROL = 33,
            APPLICATION_MESSAGE = 48,
            LAUNCHER = 49,
            LOGS = 2000,
            PING = 2001,
            DRAW = 2002,
            RESET = 2003,
            APP = 2004,
            NOTIFICATION = 3000,
            RESOURCE = 4000,
            SYS_REG = 5000,
            FCT_REG = 5001,
            APP_INSTALL_MANAGER = 6000,
            RUNKEEPER = 7000,
            PUT_BYTES = 48879,
            MAX_ENDPOINT = 65535
        };
    };

    PebbleMessage();
    explicit PebbleMessage(uint id);

    QByteArray pack();

    void setEndpoint(uint);
    void setCookie(quint32);

    void sender(QString);
    void subject(QString);
    void body(QString);
    void timestamp(QDateTime);

    static QByteArray packMessageData(QStringList);
    static QByteArray packMessageData(QStringList, quint8);

private:

    quint16 m_endpoint;
    quint32 m_cookie;

    QByteArray m_data;

    QString m_sender;
    QString m_subject;
    QString m_body;

    QDateTime m_timestamp;
};

#endif // PEBBLEMESSAGE_H
