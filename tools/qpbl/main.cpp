#include <QCoreApplication>
#include <QCommandLineParser>

#include <QDebug>

#include "pebble.h"

void runCommand(Pebble &p, QString command, const QStringList args) {
    if(command.toLower() == "ping") {
        p.ping(0xdeadbeef);
        return;
    } else if(command.toLower() == "notify") {
        if(args.length() < 3) {
            qDebug() << "A notification needs 3 parameters, sender subject and body";
            return;
        }
        QString sender = args[0];
        QString subject = args[1];
        QString body = args[2];

        p.notify(sender, subject, body);
    } else {
        qDebug() << "Unknown command:" << command;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("qpbl");
    QCoreApplication::setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("A Pebble smartwatch tool");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption idOption(QStringList() << "i" << "id",
                                "Pebble ID or bluetooth MAC of your pebble",
                                "id");
    parser.addOption(idOption);
    parser.process(a);

    QStringList args = parser.positionalArguments();

    QString command = "ping";

    QString id = parser.value("id");
    if(id.isEmpty() && !qgetenv("PEBBLE_ID").isEmpty()) {
        id = qgetenv("PEBBLE_ID").constData();
    }

    if(!args.isEmpty()) {
        command = args.takeFirst();
    }

    if(id.isEmpty()) {
        qDebug() << "You must supply your pebble ID, bluetooth address.";
        qDebug() << "Either as the first parameter or through the PEBBLE_ID environement variable.";
        return 1;
    }

    Pebble p(id);

    QObject::connect(&p, &Pebble::readyChanged, [&p, command, args](bool ready){
        if(ready) {
            runCommand(p, command, args);
//            p.disconnect();
        }
    });

    QObject::connect(&p, &Pebble::disconnected, [](){
        qApp->quit();
    });

    p.connect();

    return a.exec();
}
