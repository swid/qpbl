TEMPLATE = app
TARGET = qpbl

include(../../qpbl.pri)

INCLUDEPATH += .

CONFIG += C++11

DESTDIR = $$top_builddir

# Input
SOURCES += main.cpp

QMAKE_RPATHDIR += $$top_builddir/src
