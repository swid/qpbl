#include <QDebug>
#include <QDBusMessage>

#include "receiver.h"

Receiver::Receiver(Pebble *p, QObject *parent) :
    QObject(parent),
    p(p)
{
}

Receiver::~Receiver()
{
    delete(p);
}

uint Receiver::Notify(const QString &app_name,
                      uint replaces_id,
                      const QString &app_icon,
                      const QString &summary,
                      const QString &body,
                      const QStringList &actions,
                      const QVariantHash &hints,
                      int expire_timeout)
{
    Q_UNUSED(replaces_id)
    p->notify(app_name, summary, body);
    return 0;
}
