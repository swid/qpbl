#ifndef RECEIVER_H
#define RECEIVER_H

#include <QObject>
#include <QDBusVirtualObject>

#include <pebble.h>

class Receiver : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.freedesktop.Notifications")
public:
    explicit Receiver(Pebble *p, QObject *parent = 0);
    ~Receiver();

signals:

public slots:
    uint Notify(const QString &app_name, uint replaces_id, const QString &app_icon, const QString &summary, const QString &body, const QStringList &actions, const QVariantHash &hints, int expire_timeout);

private:
    Pebble *p;

};

#endif // RECEIVER_H
