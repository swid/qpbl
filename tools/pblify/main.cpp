#include <QCoreApplication>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QTextStream>
#include <QDebug>

#include <pebble.h>

#include "receiver.h"

void usage() {
    QTextStream(stdout) << "Usage: pblify <Pebble ID|Bluetooth MAC>" << endl;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("pblify");
    QCoreApplication::setApplicationVersion("0.1");

    // See: https://github.com/ruedigergad/SkippingStones/blob/master/dbusadapter.cpp
    // See: http://stackoverflow.com/questions/22592042/qt-dbus-monitor-method-calls
    QDBusConnection bus = QDBusConnection::sessionBus();

    qDebug() << a.arguments();

    if(a.arguments().length() < 2) {
        usage();
        return 1;
    }

    QString pebbleID = a.arguments()[1];

    Pebble *p;
    if(pebbleID.length() == 4) {
        p = new Pebble(a.arguments()[1]);
    } else {
        p = new Pebble(QBluetoothAddress(pebbleID));
    }

    p->connect();

    Receiver rcv(p);

    bus.registerObject("/org/freedesktop/Notifications", &rcv, QDBusConnection::ExportAllSlots);

    QString matchString = "interface='org.freedesktop.Notifications',member='Notify',type='method_call',eavesdrop='true'";
    QDBusInterface busInterface("org.freedesktop.DBus",
                                "/org/freedesktop/DBus",
                                "org.freedesktop.DBus");
    busInterface.call("AddMatch", matchString);

    return a.exec();
}
