TEMPLATE = app
TARGET = pblify
INCLUDEPATH += .

QT += dbus

include(../../qpbl.pri)

# Input
SOURCES += main.cpp \
    receiver.cpp

HEADERS += \
    receiver.h
