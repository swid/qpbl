include(../qpbl.pri)

TEMPLATE = app
TARGET = test
INCLUDEPATH += . $$PWD/../src/

QT += testlib
CONFIG += testcase

# Input
SOURCES += test_pebblemessage.cpp

QMAKE_RPATHDIR += $$top_builddir/src
