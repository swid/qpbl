#include <QTest>

#include "../src/pebblemessage.h"

class TestPebblemessage: public QObject
{
    Q_OBJECT

private slots:
    void test_ping_pack();
    void test_message_packing();
    void test_notification_pack();
};

void TestPebblemessage::test_ping_pack()
{
    PebbleMessage m(PebbleMessage::Endpoints::PING);
    m.setCookie(0xdeadbeef);

    QString cmp = m.pack().toHex();
    QCOMPARE(cmp, QString("000507d100deadbeef"));
}

void TestPebblemessage::test_notification_pack()
{
    PebbleMessage m(PebbleMessage::Endpoints::NOTIFICATION);

    m.sender("sender");
    m.subject("subject");
    m.body("body");
    m.timestamp(QDateTime::fromMSecsSinceEpoch(1418594270000));

    QString cmp = m.pack().toHex();
    QCOMPARE(cmp,
             QString("00230bb800067365"
                     "6e64657204626f64"
                     "790d313431383539"
                     "3432373030303007"
                     "7375626a656374"));
}

void TestPebblemessage::test_message_packing()
{
    QString res = PebbleMessage::packMessageData(QStringList() << "test" << "123").toHex();
    QCOMPARE(res,
             QString("00047465737403313233"));
    res = PebbleMessage::packMessageData(QStringList()
                                         << "the sender"
                                         << "a message body"
                                         << "1418586987000"
                                         << "a subject line"
                                         ).toHex();
    QCOMPARE(res,
             QString("000a746865207365"
                     "6e6465720e61206d"
                     "6573736167652062"
                     "6f64790d31343138"
                     "3538363938373030"
                     "300e61207375626a"
                     "656374206c696e65"));
}

QTEST_MAIN(TestPebblemessage)
#include "test_pebblemessage.moc"
